# Symulation of elastic objects with elements of molecular dynamics

## Dependencies
- Python 3 with installed **tkinter** library

### Windows
**tkinter** comes preinstalled with python3 interpreter for Windows, you can run code out of the box

### Linux
**tkinter** usually comes preinstalled but if it isn't the case for you, try using these commands:
```bash
Debian / Ubuntu:
sudo apt-get install python3-tk

Fedora:
sudo dnf install python3-tkinter

Then using pip install numpy:
pip3 install numpy
```

## How to run?
While in *python_implementation* directory run command:
```bash
python3 start.py
```