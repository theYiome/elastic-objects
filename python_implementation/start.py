import math
import tkinter
import symulations
import common
import time
import sys
import json


def main():
    with open("config.json", "r") as fp:
        config = json.load(fp)

    root = tkinter.Tk()

    # initialization
    window_width = 800
    window_height = 800

    symulation_object = symulations.Symulation01(config)
    print("dx:", symulation_object.balance_distance)
    print(symulation_object.calculate_energy())
    print(symulation_object.calculate_squeeze_force())
    print("mi:", symulation_object.mi)

    canvas = tkinter.Canvas(root, bg="black", height=window_width, width=window_height)

    def onButtonClick(event):
        code = event.keysym
        if code == "Up":
            symulation_object.gravity_unit_vector = common.Vector2D(0, -1)
        elif code == "Down":
            symulation_object.gravity_unit_vector = common.Vector2D(0, 1)
        elif code == "Right":
            symulation_object.gravity_unit_vector = common.Vector2D(1, 0)
        elif code == "Left":
            symulation_object.gravity_unit_vector = common.Vector2D(-1, 0)

    root.bind("<Key>", onButtonClick)

    # how many times symultaion should iterate for one draw call
    steps_per_frame = config["steps_per_frame"]
    dt = math.pow(10, config["dt"])

    # data collection
    symulation_time = 0
    f = open("logs/log.txt", "w")
    start_time = time.time()

    while True:

        # running symulation
        for _ in range(steps_per_frame):
            # symulation_object.next_step_euler(dt)
            symulation_object.next_step_rk4(dt)
            # symulation_object.next_step_varlet(dt)
        
        if symulation_time >= config["t_max"]:
            print("Symulation time: ", time.time() - start_time)
            sys.exit(0)

        symulation_time += steps_per_frame * dt
        e = symulation_object.calculate_energy()

        f.write("{} {} {} {} {} {} {} {} {} {}\n".format(symulation_time, 0, *e))
        

        # drawing and managing aplication
        canvas.delete("all")
        symulation_object.draw(canvas)

        canvas.pack()

        # updating window, reading events
        root.update_idletasks()
        root.update()


if __name__ == "__main__":
    main()
