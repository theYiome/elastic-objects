import math

f_ptr = open("plots/bounce_dt4.txt", "r")

count = 0
mse = 0
Ep = None
for line in f_ptr:
    count += 1
    E = float(line.split(" ")[6])
    if Ep is None:
        Ep = E
    else:
        mse += math.pow(Ep - E, 2)

print(mse / count)