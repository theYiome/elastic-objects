import math
from common import Particle
from common import Vector2D
import numpy as np


def gravity(gravity_unit_vector: Vector2D(), mass: float, gravity_constant: float):
    return gravity_unit_vector * mass * gravity_constant


def elastic_numpy(index: int, atoms: np.array, neighbours: np.array, k: float) -> Vector2D():
    a = atoms[index]
    net_force = Vector2D(0, 0)
    N = len(neighbours[index])

    for i in range(N):
        dx = neighbours[index][i]

        if dx != 0:
            b = atoms[i]
            diff = (b.pos - a.pos)
            l = (b.pos - a.pos).magnitude()
            net_force += diff * k * ((l - dx) / l)

    return net_force


def friction(velocity: Vector2D(), k: float) -> Vector2D():
    return -(velocity * k)


def lennard_jones_numpy(index: int, atoms: np.array, neighbours: np.array, V0: float) -> Vector2D():
    a = atoms[index]
    net_force = Vector2D(0, 0)
    N = len(neighbours[index])

    for i in range(N):
        dx = neighbours[index][i]

        if dx != 0:
            b = atoms[i]

            l = (b.pos - a.pos).magnitude()
            unit_vector = (b.pos - a.pos).normalized()

            x = math.pow(dx / l, 13) - math.pow(dx / l, 7)
            net_force += unit_vector * 3 * (V0 / dx) * x

    return net_force


def wall_repulsion(a: Particle, dx: float, U0: float, box_size: float) -> Vector2D():
    x0 = 0
    x1 = box_size
    y0 = 0
    y1 = box_size

    p = 2
    x = a.pos.x
    y = a.pos.y

    k = (U0 * p) / dx

    if x < x0 + dx and x > x0 - dx:
        a1 = k * math.pow((x - (x0 - dx)) / dx, p - 1)
    else:
        a1 = 0

    if x > x1 - dx and x < x1 + dx:
        a2 = k * math.pow((x - (x1 + dx)) / dx, p - 1)
    else:
        a2 = 0

    if y < y0 + dx and y > y0 - dx:
        a3 = k * math.pow((y - (y0 - dx)) / dx, p - 1)
    else:
        a3 = 0

    if y > y1 - dx and y < y1 + dx:
        a4 = k * math.pow((y - (y1 + dx)) / dx, p - 1)
    else:
        a4 = 0

    return Vector2D(a1 + a2, a3 + a4)


def net_acceleration(index: int, atoms: np.array, neighbours, V0=0, wall_dx=0, U0=0, box_size=0, gravity_unit_vector=Vector2D(0, 1), G=0, friction_coefficient=0, elasticity_coefficient=0):

    a = atoms[index]

    net_force = Vector2D(0, 0)

    if G != 0:
        net_force += gravity(gravity_unit_vector, a.mass, G)

    if V0 != 0:
        net_force += lennard_jones_numpy(index, atoms, neighbours, V0)

    if friction_coefficient != 0:
        net_force += friction(a.vel, friction_coefficient)

    if U0 != 0:
        net_force += wall_repulsion(a, wall_dx, U0, box_size)

    if elasticity_coefficient != 0:
        net_force += elastic_numpy(index, atoms, neighbours, elasticity_coefficient)

    return net_force / a.mass


def squeeze_acceleration(index: int, atoms: np.array, ny: int, Fs: float):

    a = atoms[index]

    net_force = Vector2D(0, 0)

    if index % ny == 0:
        net_force += Vector2D(0, 1) * Fs
    elif index % ny == (ny - 1):
        net_force += Vector2D(0, -1) * Fs

    return net_force / a.mass