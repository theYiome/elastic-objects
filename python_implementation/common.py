import math


class Vector2D:
    def __init__(self, x=0.0, y=0.0):
        self.x = x
        self.y = y

    def __add__(self, v2):
        return Vector2D(self.x + v2.x, self.y + v2.y)

    def __sub__(self, v2):
        return Vector2D(self.x - v2.x, self.y - v2.y)

    def __mul__(self, scalar):
        return Vector2D(self.x * scalar, self.y * scalar)

    def __truediv__(self, scalar):
        return Vector2D(self.x / scalar, self.y / scalar)

    def __neg__(self):
        return Vector2D(-self.x, -self.y)

    def magnitude(self):
        return math.sqrt(math.pow(self.x, 2) + math.pow(self.y, 2))

    def normalized(self):
        return self / self.magnitude()


class Particle:
    def __init__(self, mass, pos, vel=Vector2D(0, 0)):
        self.pos = pos
        self.vel = vel
        self.mass = mass

    def draw(self, canvas, radius=0.003):
        r = radius
        x = self.pos.x
        y = self.pos.y

        clip_distance = 100
        if x > clip_distance or x < -clip_distance or y > clip_distance or y < -clip_distance:
            # if object is too far away to be seen there is no need to draw it
            return None
        else:
            # draw
            return canvas.create_oval(x - r, y - r, x + r, y + r, fill="white")
