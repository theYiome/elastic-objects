import math
from operator import indexOf
import time
import random

from numpy.core.fromnumeric import squeeze
from common import Vector2D
from common import Particle
import algorithms
import force
from force import gravity
import copy
import numpy as np


class Symulation01:

    def build_grid(self, config):
        # grid creation
        x_offset = config["x_offset"]
        y_offset = config["y_offset"]
        nx = config["nx"]
        ny = config["ny"]
        alpha = config["alpha"]
        w = config["w"]
        d = config["d"]
        ro = config["ro"]

        self.nx = nx
        self.ny = ny

        self.N = nx * ny

        delta = w / (nx - 1)

        self.balance_distance = delta

        h = delta * (ny - 1)

        M = d * w * h * ro

        mi = M / (nx * ny)
        self.mi = mi

        self.atoms = np.empty(self.N, dtype=Particle)

        body_center = Vector2D(x_offset + (nx / 2) * delta, y_offset + (ny / 2) * delta)

        for i in range(nx):
            for j in range(ny):
                index = (i * ny) + j

                pos = Vector2D(x_offset + i * delta, y_offset + j * delta)
                pos = pos - body_center
                
                beta = math.pi * alpha
                pos = Vector2D(pos.x * math.cos(beta) - pos.y * math.sin(beta), pos.x * math.sin(beta) + pos.y * math.cos(beta))

                pos = pos + body_center
                self.atoms[index] = Particle(mi, pos)

        # self.neighbours = algorithms.find_neighbours(self.atoms, delta * 1.5)
        self.neighbours_matrix = algorithms.find_neighbours_numpy(self.atoms, delta * 1.5)

        # memory allocation
        # rk4
        self.rk4_tmp_atoms = np.empty(self.N, dtype=Particle)
        self.k1_r = np.empty(self.N, dtype=Vector2D)
        self.k1_v = np.empty(self.N, dtype=Vector2D)

        self.k2_r = np.empty(self.N, dtype=Vector2D)
        self.k2_v = np.empty(self.N, dtype=Vector2D)

        self.k3_r = np.empty(self.N, dtype=Vector2D)
        self.k3_v = np.empty(self.N, dtype=Vector2D)

        self.k4_r = np.empty(self.N, dtype=Vector2D)
        self.k4_v = np.empty(self.N, dtype=Vector2D)

    def __init__(self, config):
        # settings
        self.gravity_unit_vector = Vector2D(0, 1)
        self.build_grid(config)

        self.box_size = 0.2 # 0.05
        self.elasticity_coefficient = 0 # 10000

        self.V0 = config["V0"] # -0.03
        self.U0 = config["U0"] # 0.05
        self.G = config["G"] # 9.81
        self.friction_coefficient = config["friction_coefficient"] # 0.4

        self.inital_lennjon_energy = None
        self.squezee_coefficient = 0

    def calculate_acceleration(self, index, atoms=None):
        if atoms is None:
            atoms = self.atoms
        
        net = Vector2D(0, 0)

        net += force.net_acceleration(index, atoms,
                self.neighbours_matrix, V0=self.V0, wall_dx=0.005, U0=self.U0, box_size=self.box_size,
                gravity_unit_vector=self.gravity_unit_vector, G=self.G,
                friction_coefficient=self.friction_coefficient, elasticity_coefficient=self.elasticity_coefficient
            )
        
        # net += force.squeeze_acceleration(index, atoms, self.ny, self.squezee_coefficient)

        return net


    def next_step_rk4(self, dt: float):
        atoms = self.atoms
        self.rk4_tmp_atoms = copy.deepcopy(atoms)

        # k1
        for index, a in enumerate(atoms):
            self.k1_r[index] = a.vel
            self.k1_v[index] = self.calculate_acceleration(index)
        
        # change position 1
        for index, a in enumerate(self.rk4_tmp_atoms):
            a.pos = atoms[index].pos + self.k1_r[index] * (dt * 0.5)
            a.vel = atoms[index].vel + self.k1_v[index] * (dt * 0.5)

        # k2
        for index, a in enumerate(atoms):
            self.k2_r[index] = atoms[index].vel + self.k1_v[index] * (dt * 0.5)
            self.k2_v[index] = self.calculate_acceleration(index, self.rk4_tmp_atoms)

        # change position 2
        for index, a in enumerate(self.rk4_tmp_atoms):
            a.pos = atoms[index].pos + self.k2_r[index] * (dt * 0.5)
            a.vel = atoms[index].vel + self.k2_v[index] * (dt * 0.5)

        # k3
        for index, a in enumerate(atoms):
            self.k3_r[index] = atoms[index].vel + self.k2_v[index] * (dt * 0.5)
            self.k3_v[index] = self.calculate_acceleration(index, self.rk4_tmp_atoms)

        # change position 3
        for index, a in enumerate(self.rk4_tmp_atoms):
            a.pos = atoms[index].pos + self.k3_r[index] * dt
            a.vel = atoms[index].vel + self.k3_v[index] * dt

        # k4
        for index, a in enumerate(atoms):
            self.k4_r[index] = atoms[index].vel + self.k3_v[index] * dt
            self.k4_v[index] = self.calculate_acceleration(index, self.rk4_tmp_atoms)

        # calculate new positions
        for i, a in enumerate(atoms):
            a.pos += (self.k1_r[i] + (self.k2_r[i] * 2) + (self.k3_r[i] * 2) + self.k4_r[i]) * (dt / 6)
            a.vel += (self.k1_v[i] + (self.k2_v[i] * 2) + (self.k3_v[i] * 2) + self.k4_v[i]) * (dt / 6)

    def next_step_euler(self, dt: float):
        for index, a in enumerate(self.atoms):
            a.vel += self.calculate_acceleration(index) * dt
        
        for a in self.atoms:
            a.pos += a.vel * dt

    def next_step_varlet(self, dt: float):
        for index, a in enumerate(self.atoms):
            acc = self.calculate_acceleration(index, self.atoms)
            a.pos += (a.vel * dt) + acc * (dt * dt * 0.5)

            new_acc = self.calculate_acceleration(index, self.atoms)
            a.vel += (new_acc + acc) * (dt * 0.5)
    
    def draw(self, canvas):
        zoom_factor = 3400
        zoom_offset = -0.005
        box_thickness = 0.005
        box_size = self.box_size
        canvas.create_rectangle(0, 0 - box_thickness, box_size, 0, fill="white")
        canvas.create_rectangle(0, box_size + box_thickness, box_size, box_size, fill="white")
        canvas.create_rectangle(0 - box_thickness, 0, 0, box_size, fill="white")
        canvas.create_rectangle(box_size + box_thickness, 0, box_size, box_size, fill="white")

        for a in self.atoms:
            a.draw(canvas)

        canvas.scale("all", zoom_offset * 3, zoom_offset * 3, zoom_factor, zoom_factor)

    def calculate_energy(self):

        kinetic_energy = 0
        gravity_energy = 0
        elastic_energy = 0
        lennjon_energy = 0

        for a in self.atoms:
            kinetic_energy += a.mass * math.pow(a.vel.magnitude(), 2) * 0.5

        for a in self.atoms:
            gravity_energy += a.mass * self.G * (self.box_size - a.pos.y)

        for i, a in enumerate(self.atoms):
            for j in range(0, self.N):
                
                dx = self.neighbours_matrix[i][j]
                
                if dx != 0:
                    b = self.atoms[j]
                    dist = (b.pos - a.pos).magnitude()

                    if self.elasticity_coefficient is not None:
                        elastic_energy += (self.elasticity_coefficient * 0.5) * math.pow(dist - dx, 2)

                    if self.V0 is not None:
                        m01 = math.pow(2, 1 / 6)
                        sigma = (1 / m01) * dx
                        lennjon_energy += self.V0 * (pow(sigma / dist, 12) - pow(sigma / dist, 6))

        if self.inital_lennjon_energy is None:
            self.inital_lennjon_energy = lennjon_energy

        sample_atom = self.atoms[0]

        lje = (lennjon_energy - self.inital_lennjon_energy) * (-1 / 2)
        ee = elastic_energy / 2
        net_energy = kinetic_energy + gravity_energy + lje # + ee

        return (kinetic_energy, gravity_energy, lje, ee, net_energy, sample_atom.pos.x, self.box_size - sample_atom.pos.y, self.calculate_width())

    def calculate_width(self):
        top_index = 0 + self.ny
        bottom_index = (self.ny - 1) + self.ny

        a = self.atoms[top_index]
        b = self.atoms[bottom_index]
        return (b.pos - a.pos).magnitude()
    
    def calculate_squeeze_force(self):
        return self.nx * self.squezee_coefficient


