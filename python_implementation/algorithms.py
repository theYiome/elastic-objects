import math
import copy
from common import Particle
from common import Vector2D
import force
import numpy as np


def find_neighbours(atoms, Rs):
    Nw = len(atoms)
    neighbours = list()

    for i in range(Nw):
        # znajdowanie wszystkich sasiadow atom(i)
        current_neighbours = list()
        for j in range(Nw):
            if i != j:
                # odleglosc pomiedzy atom(i) oraz atom(j)
                dist = (atoms[j].pos - atoms[i].pos).magnitude()

                if dist < Rs:
                    current_neighbours.append(j)

        neighbours.append(current_neighbours)

    return neighbours


def find_neighbours_numpy(atoms, search_distance):
    N = len(atoms)
    neighbours = np.zeros((N, N))

    for i in range(N):
        for j in range(N):
            if i != j:
                dist = (atoms[j].pos - atoms[i].pos).magnitude()
                if dist < search_distance:
                    neighbours[i][j] = dist
    
    return neighbours