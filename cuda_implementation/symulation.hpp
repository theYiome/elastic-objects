#pragma once

#include "common.hpp"
#include "const.hpp"

struct Symulation {

    Particle *particles;
    float *neighbours;
    Vector2D *acceleration;
    int N;
    float balance_distance;

    void find_neighbours(float search_distance);

    Symulation();
    ~Symulation();
    
    void cpu_next_step_euler(float dt, int steps_amount);
    void gpu_next_step_euler(float dt, int steps_amount);
};