#include <math.h>

#include "const.hpp"
#include "common.hpp"

void cpu_calculate_acceleration(int index, int N, Particle *particles, float *neighbours, Vector2D *acceleration) {
    acceleration[index].x = 0;
    acceleration[index].y = 0;
    
    if (index < N) {
        // gravity
        acceleration[index] += Vector2D(0, 1) * particles[index].mass * G;

        // friction
        acceleration[index] += particles[index].vel * friction_coefficient;

        // wall repulsion
        {
            float a1 = 0, a2 = 0, a3 = 0, a4 = 0;
            float x0 = 0, y0 = 0;
            float x1 = box_size, y1 = box_size;
            float p = 2;
    
            float x = particles[index].pos.x;
            float y = particles[index].pos.y;
    
            float k = (U0 * p) / wall_dx;
    
            if(x < x0 + wall_dx && x > x0 - wall_dx) {
                a1 = k * pow((x - (x0 - wall_dx)) / wall_dx, p - 1);
            }
    
            if(x > x1 - wall_dx && x < x1 + wall_dx) {
                a2 = k * pow((x - (x1 + wall_dx)) / wall_dx, p - 1);
            }
    
            if(y < y0 + wall_dx && y > y0 - wall_dx) {
                a3 = k * pow((y - (y0 - wall_dx)) / wall_dx, p - 1);
            }
    
            if(y > y1 - wall_dx && y < y1 + wall_dx) {
                a4 = k * pow((y - (y1 + wall_dx)) / wall_dx, p - 1);
            }
    
            acceleration[index] += Vector2D(a1 + a2, a3 + a4);
        }

        // lennard jones
        {
            float dx, l;
            Vector2D unit_vector;

            for(int i = 0; i < N; ++i) {
                dx = neighbours[index * N + i];
                if(dx != 0) {
                    l = (particles[i].pos - particles[index].pos).magnitude();
                    unit_vector = (particles[i].pos - particles[index].pos).normalize();

                    acceleration[index] += unit_vector * 3 * (V0 / dx) * (pow(dx / l, 13) - pow(dx / l, 7));
                }
            }
        }

        acceleration[index] /= particles[index].mass;

    }

    return;
}

void cpu_integrate_euler(int index, int N, Particle *particles, Vector2D *acceleration, float dt) {
    particles[index].vel += acceleration[index] * dt;
    particles[index].pos += particles[index].vel * dt;
}

__global__ void gpu_calculate_acceleration(int N, Particle *particles, float *neighbours, Vector2D *acceleration) {
    
    const int index = threadIdx.x + blockIdx.x * blockDim.x;

    acceleration[index].x = 0;
    acceleration[index].y = 0;
    
    if (index < N) {
        // gravity
        // acceleration[index].x += 0 * particles[index].mass * G;
        acceleration[index].y += 1 * particles[index].mass * G;

        // friction
        acceleration[index].x += particles[index].vel.x * friction_coefficient;
        acceleration[index].y += particles[index].vel.y * friction_coefficient;

        // wall repulsion
        {
            float a1 = 0, a2 = 0, a3 = 0, a4 = 0;
            float x0 = 0, y0 = 0;
            float x1 = box_size, y1 = box_size;
            float p = 2;
            float p2 = p - 1.f;
    
            float x = particles[index].pos.x;
            float y = particles[index].pos.y;
    
            float k = (U0 * p) / wall_dx;
    
            if(x < x0 + wall_dx && x > x0 - wall_dx) {
                a1 = k * pow((x - (x0 - wall_dx)) / wall_dx, p2);
            }
    
            if(x > x1 - wall_dx && x < x1 + wall_dx) {
                a2 = k * pow((x - (x1 + wall_dx)) / wall_dx, p2);
            }
    
            if(y < y0 + wall_dx && y > y0 - wall_dx) {
                a3 = k * pow((y - (y0 - wall_dx)) / wall_dx, p2);
            }
    
            if(y > y1 - wall_dx && y < y1 + wall_dx) {
                a4 = k * pow((y - (y1 + wall_dx)) / wall_dx, p2);
            }
    
            acceleration[index].x += a1 + a2;
            acceleration[index].y += a3 + a4;
        }

        // lennard jones
        {
            float dx, l, uv_x, uv_y, x0, y0, factor;

            for(int i = 0; i < N; ++i) {
                dx = neighbours[index * N + i];
                if(dx != 0) {
                    x0 = particles[i].pos.x - particles[index].pos.x;
                    y0 = particles[i].pos.y - particles[index].pos.y;
                    l = sqrt(pow(x0, 2.f) + pow(y0, 2.f));
                    factor = 3.f * (V0 / dx) * (pow(dx / l, 13.f) - pow(dx / l, 7.f));

                    uv_x = x0 / l;
                    uv_y = y0 / l;

                    acceleration[index].x += uv_x * factor;
                    acceleration[index].y += uv_y * factor;
                }
            }
        }

        acceleration[index].x /= particles[index].mass;
        acceleration[index].y /= particles[index].mass;
    }

    return;
}

__global__ void gpu_integrate_euler(int N, Particle *particles, Vector2D *acceleration, float dt) {
    
    const int index = threadIdx.x + blockIdx.x * blockDim.x;
    
    if (index < N) {
        particles[index].vel.x += acceleration[index].x * dt;
        particles[index].vel.y += acceleration[index].y * dt;

        particles[index].pos.x += particles[index].vel.x * dt;
        particles[index].pos.y += particles[index].vel.y * dt;
    }

    return;
}