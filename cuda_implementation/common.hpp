#pragma once

struct Vector2D {
    float x, y;

    Vector2D(const float & _x = 0, const float & _y = 0);

    Vector2D operator+(const Vector2D & b);

    Vector2D & operator+=(const Vector2D & b);

    Vector2D & operator/=(const float & div);

    Vector2D operator-(const Vector2D & b);

    Vector2D operator-();

    Vector2D operator*(const float & mul);

    float magnitude();

    Vector2D & normalize();
};

struct Particle {
    Vector2D pos;
    Vector2D vel;
    float mass;

    Particle(const float & _mass, const Vector2D & _pos, const Vector2D & _vel = Vector2D(0, 0));
};