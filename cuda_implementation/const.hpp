#pragma once 

// force constants
#define wall_dx 0.005f
#define U0 0.05f

#define G 9.81f
#define friction_coefficient 0.002f

#define V0 -0.03f

// grid definition
#define box_size 0.8f

#define nx 32
#define ny 48

#define x_offset 0.1f
#define y_offset 0.1f

#define width 0.09f
#define depth 0.01f
#define ro 1000.0f

#define search_ratio 1.5f