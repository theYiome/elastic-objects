#pragma once

#include "common.hpp"

void cpu_calculate_acceleration(int index, int N, Particle *particles, float *neighbours, Vector2D *acceleration);

void cpu_integrate_euler(int index, int N, Particle *particles, Vector2D *acceleration, float dt);

__global__ void gpu_calculate_acceleration(int N, Particle *particles, float *neighbours, Vector2D *acceleration);

__global__ void gpu_integrate_euler(int N, Particle *particles, Vector2D *acceleration, float dt);