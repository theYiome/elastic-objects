#define M_PI 3.14159265358979323846

#include "symulation.hpp"
#include "force.hpp"

void Symulation::find_neighbours(float search_distance) {
    for(int i = 0; i < N; ++i) {
        for(int j = 0; j < N; ++j) {
            if(i != 0) {
                float dist = (particles[j].pos - particles[i].pos).magnitude();
                if(dist < search_distance) {
                    neighbours[(i * N) + j] = dist;
                }
            }
        }
    }
}

Symulation::Symulation() {
    N = nx * ny;

    float dx = width / (nx - 1.0);

    float height = dx * (ny - 1.0);

    float total_mass = depth * width * height * ro;
    float mi = total_mass / N;

    Vector2D body_center = Vector2D(x_offset + (nx / 2.0) * dx, y_offset + (ny / 2.0) * dx);

    // bulid grid
    cudaMallocManaged(&particles, sizeof(Particle) * N);

    for(int i = 0; i < nx; ++i) {
        for(int j = 0; j < ny; ++j) {

            Vector2D pos = Vector2D(x_offset + i * dx, y_offset + j * dx);
            pos = pos - body_center;
            
            float alpha = M_PI * 0.2;
            pos = Vector2D(pos.x * cos(alpha) - pos.y * sin(alpha), pos.x * sin(alpha) + pos.y * cos(alpha));
            
            pos = pos + body_center;
            
            int index = (i * ny) + j;
            particles[index] = Particle(mi, pos);
        }
    }

    // build neighbours matrix
    cudaMallocManaged(&neighbours, sizeof(float) * N * N);

    float search_distance = dx * search_ratio;

    for(int i = 0; i < N; ++i) {
        for(int j = 0; j < N; ++j) {
            if(i != 0) {
                float dist = (particles[j].pos - particles[i].pos).magnitude();
                if(dist < search_distance) {
                    int index = (N * i) + j;
                    neighbours[index] = dist;
                }
            }
        }
    }

    cudaMallocManaged(&acceleration, sizeof(Vector2D) * N);
}

Symulation::~Symulation() {
    cudaFree(&particles);
}

void Symulation::cpu_next_step_euler(float dt, int steps_amount) {
    for(int iter = 0; iter < steps_amount; ++iter) {

        for(int i = 0; i < N; ++i) {
            cpu_calculate_acceleration(i, N, particles, neighbours, acceleration);
        }

        for(int i = 0; i < N; ++i) {
            cpu_integrate_euler(i, N, particles, acceleration, dt);
        }
    }
}

void Symulation::gpu_next_step_euler(float dt, int steps_amount) {
    int deviceId;
    int numberOfSMs;
    
    cudaGetDevice(&deviceId);
    cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
    

    size_t threadsPerBlock = 256;
    size_t numberOfBlocks = (N / threadsPerBlock) + 1;

    cudaMemPrefetchAsync(particles, sizeof(Particle) * N, deviceId);

    for(int iter = 0; iter < steps_amount; ++iter) {
        gpu_calculate_acceleration<<<numberOfBlocks, threadsPerBlock>>>(N, particles, neighbours, acceleration);
        gpu_integrate_euler<<<numberOfBlocks, threadsPerBlock>>>(N, particles, acceleration, dt);
    }

    cudaMemPrefetchAsync(particles, sizeof(Particle) * N, cudaCpuDeviceId);
}