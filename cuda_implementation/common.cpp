#pragma once

#include <math.h>

#include "common.hpp"

// Vector2D
Vector2D::Vector2D(const float & _x, const float & _y) {
    x = _x;
    y = _y;
}

Vector2D Vector2D::operator+(const Vector2D & b) {
    return Vector2D(x + b.x, y + b.y);
}

Vector2D & Vector2D::operator+=(const Vector2D & b) {
    x += b.x;
    y += b.y;
    return *this;
}

Vector2D & Vector2D::operator/=(const float & div) {
    x /= div;
    y /= div;
    return *this;
}

Vector2D Vector2D::operator-(const Vector2D & b) {
    return Vector2D(x - b.x, y - b.y);
}

Vector2D Vector2D::operator-() {
    return Vector2D(-x, -y);
}

Vector2D Vector2D::operator*(const float & mul) {
    return Vector2D(x * mul, y * mul);
}

float Vector2D::magnitude() {
    return sqrt(pow(x, 2) + pow(y, 2));
}

Vector2D & Vector2D::normalize() {
    float l = magnitude();
    if(l != 0) {
        x /= l;
        y /= l;
    }
    return *this;
}

// Particle
Particle::Particle(const float & _mass, const Vector2D & _pos, const Vector2D & _vel) {
    pos = _pos;
    vel = _vel;
    mass = _mass;
}