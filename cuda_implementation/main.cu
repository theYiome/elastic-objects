#include <stdio.h>
#include <chrono>

#include "symulation.hpp"

int main(const int argc, const char** argv) {
    printf("Elastic body symulation using CPU and GPU\n");

    const unsigned int steps_amount = 10000;
    const float dt = pow(10, -6);

    Symulation cpu_symulation;
    Symulation gpu_symulation;

    printf("\nStarting time measurement\n");

    // time measurement init
    std::chrono::steady_clock::time_point begin, end;
    float ms;

    begin = std::chrono::steady_clock::now();
    cpu_symulation.cpu_next_step_euler(dt, steps_amount);
    end = std::chrono::steady_clock::now();

    ms = (std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()) / 1000.f;
    printf("\nCalculation on CPU took %f milliseconds for %u steps\n", ms, steps_amount);
    
    begin = std::chrono::steady_clock::now();
    gpu_symulation.gpu_next_step_euler(dt, steps_amount);
    end = std::chrono::steady_clock::now();
    
    ms = (std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()) / 1000.f;
    printf("\nCalculation on GPU took %f milliseconds for %u steps\n", ms, steps_amount);
}